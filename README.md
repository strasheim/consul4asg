## Consul 4 AutoScalingGroups 
Health checks in consul generate events. Those events you can watch -> [Consul Documentation] (https://www.consul.io/docs/agent/watches.html). 

Consul4ASG will if hocked to a watch readout the event and look for the AWS instanceID having that IP address. If only a single IP will be found it will send an EC2 unhealthy information.   

To enable consul4asg a few AWS settings need to match
* AWS_REGION
* AWS_ACCESS_KEY
* AWS_SECRET_ACCESS
* Role needs to have ASG write permissions 

### Warning 
Please keep in mind, that a check that is marking the instance to be unhealthy should really be sure that it's in a bad state. 
The binary only brings down the instance, it will not request a new one first. That is left to the ASG to trigger. This will cause a couple minutes without AMI out there. If you ASG runs with 1 as a default size and it's required this is NOT the tool you are looking for. 

#### How to build 
```
set GOPATH
go get -u gitlab.com/strasheim/consul4asg
binary is under $GOPATH/bin/
```

#### Consul Watches 
You will need to install consul watches to make use of this. Since here a failure in configuration has drastic effect, please note and check on what Service you attach watch on. If you don't set a service_id to a check it will be bound to a host and not a service. 


