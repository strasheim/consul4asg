// The MIT License (MIT)
//
// Copyright (c) 2017 Mark Strasheim
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/autoscaling"
	"github.com/aws/aws-sdk-go/service/ec2"
)

type WatchEvent []struct {
	Checks []struct {
		CheckID     string `json:"CheckID"`
		Name        string `json:"Name"`
		Node        string `json:"Node"`
		Notes       string `json:"Notes"`
		Output      string `json:"Output"`
		ServiceID   string `json:"ServiceID"`
		ServiceName string `json:"ServiceName"`
		Status      string `json:"Status"`
	} `json:"Checks"`
	Node struct {
		Address string `json:"Address"`
		Node    string `json:"Node"`
	} `json:"Node"`
	Service struct {
		Address string   `json:"Address"`
		ID      string   `json:"ID"`
		Port    int      `json:"Port"`
		Service string   `json:"Service"`
		Tags    []string `json:"Tags"`
	} `json:"Service"`
}

func main() {

	logwriter, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err == nil {
		log.SetFlags(0)
		log.SetOutput(logwriter)
	}

	events := make(WatchEvent, 0) // building the basic array

	bytes, err := ioutil.ReadAll(os.Stdin)
	check(err)
	err = json.Unmarshal(bytes, &events)
	check(err)

	for _, e := range events {
		for _, sp := range e.Checks {
			if sp.Status == "critical" {
				setInstanceHealth(e.Node.Address)
			}
		}
	}
}

func setInstanceHealth(ip string) {
	sess := session.Must(session.NewSession())

	svc := autoscaling.New(sess)

	params := &autoscaling.SetInstanceHealthInput{
		HealthStatus:             aws.String("Unhealthy"),
		InstanceId:               aws.String(getInstanceID(ip)),
		ShouldRespectGracePeriod: aws.Bool(true),
	}
	_, err := svc.SetInstanceHealth(params)
	check(err)

	log.Println("Termination of", ip, "is at hand")
	os.Exit(0)
}

func getInstanceID(ip string) (instId string) {

	sess := session.Must(session.NewSession())
	svc := ec2.New(sess)

	params := &ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			{
				Name: aws.String("network-interface.addresses.private-ip-address"),
				Values: []*string{
					aws.String(ip),
				},
			},
		},
	}
	resp, err := svc.DescribeInstances(params)
	check(err)

	deathCounter := 0
	for _, item := range resp.Reservations {

		for _, inst := range item.Instances {
			instId = fmt.Sprint(*inst.InstanceId)
		}
		deathCounter++
	}
	if deathCounter < 1 {
		log.Println("No Instance found with:", ip)
		os.Exit(2)
	}
	if deathCounter > 1 {
		log.Println("To many matches for", ip, " found. IP is seen ", deathCounter, "times")
		os.Exit(2)
	}
	return
}

func check(e error) {
	if e != nil {
		log.Println("Consul4ASG:", e)
		os.Exit(2)
	}
}
